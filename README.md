# SilverStripe BroadbandShop #

Gets the suppliers and subscriptions from Broadband Shop by their api.

### Requirements ###

* SilverStripe 4

### Version ###

Using Semantic Versioning.

### Installation ###

Install via Composer:

composer require "hestec/silverstripe-broadbandshop": "1.*"

### Configuration ###

Add the allowed ip addresses in mysite.yml:
```
BroadbandShopCron:
  allowed_ips:
    - '123.123.123.123'
    - '1.2.3.4.'
    - '192.168.1.1'
  ```

do a dev/build and flush.

### Working ###

Run the cron script by this url: https://www.yoursite.com/BroadbandShopCron/updateBroadbandShop

Or test with: https://www.yoursite.com/BroadbandShopCron/test

If the test gives a list of the suppliers or the errors.

### Issues ###

No known issues.

### Todo ###
