<?php

namespace Hestec\BroadbandShop;

use SilverStripe\ORM\DataObject;

class BroadbandShopSupplier extends DataObject {

    private static $singular_name = 'BroadbandShopSupplier';
    private static $plural_name = 'BroadbandShopSuppliers';

    private static $table_name = 'BroadbandShopSupplier';

    private static $db = array(
        'SupplierId' => 'Int',
        'Name' => 'Varchar(255)',
        'Description' => 'Text',
        'CustomerServiceAddress' => 'Varchar(255)',
        'CustomerServicePhone' => 'Varchar(20)',
        'CustomerServiceHours' => 'Varchar(255)',
        'ServiceAdsl' => 'Boolean',
        'ServiceVdsl' => 'Boolean',
        'ServiceCable' => 'Boolean',
        'ServiceFiber' => 'Boolean',
        'ServicePhone' => 'Boolean',
        'ServiceTv' => 'Boolean',
        'LastUpdate' => 'Datetime',
        'StillInApi' => 'Boolean',
        'Sort' => 'Int'
    );

    /*private static $has_one = array(
        'SupplierPage' => SupplierPage::class
    );*/

    private static $has_many = array(
        'BroadbandShopSubscriptions' => BroadbandShopSubscription::class
    );

    /*private static $summary_fields = array(
        'Title',
        'StartDate.Nice',
        'EndDate.Nice',
        'Enabled.Nice'
    );*/

}