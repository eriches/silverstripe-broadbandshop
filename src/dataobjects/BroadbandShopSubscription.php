<?php

namespace Hestec\BroadbandShop;

use SilverStripe\ORM\DataObject;

class BroadbandShopSubscription extends DataObject {

    private static $singular_name = 'BroadbandShopSubscription';
    private static $plural_name = 'BroadbandShopSubscriptions';

    private static $table_name = 'BroadbandShopSubscription';

    private static $db = array(
        'SubscriptionId' => 'Int',
        'SupplierId' => 'Int',
        'Category' => 'Varchar(50)',
        'Title' => 'Varchar(255)',
        'UrlId' => 'Varchar(255)',
        'OfferExclusive' => 'Boolean',
        'OfferIntro' => 'Varchar(255)',
        'OfferExtra' => 'Text',
        'OfferShort' => 'Varchar(255)',
        'OfferInternet' => 'MultiValueField',
        'OfferPhone' => 'MultiValueField',
        'OfferTv' => 'MultiValueField',
        'Discount' => 'Currency',
        'BasePrice' => 'Currency',
        'DiscountPrice' => 'Currency',
        'DiscountDuration' => 'Int',
        'YearPrice' => 'Currency',
        'ContractDuration' => 'Varchar(20)',
        'NetworkType' => 'Varchar(20)',
        'SpeedUnit' => 'Varchar(10)',
        'DownloadSpeed' => 'Int',
        'UploadSpeed' => 'Int',
        'TvChannelsTotal' => 'Int',
        'TvChannelsHd' => 'Int',
        'TvProgramMissed' => 'Text',
        'TvStartMissed' => 'Text',
        'TvMultipleTvs' => 'Text',
        'TvRecord' => 'Text',
        'TvHd' => 'Text',
        'TvVod' => 'Text',
        'PhoneUnlimited' => 'Text',
        'LastUpdate' => 'Datetime',
        'StillInApi' => 'Boolean',
        'Sort' => 'Int'
    );

    private static $has_one = array(
        //'ComparisonAllInOnePage' => ComparisonAllInOnePage::class,
        'BroadbandShopSupplier' => BroadbandShopSupplier::class
    );

    public function MbDownloadSpeed(){

        $mb = round($this->DownloadSpeed / 1000);
        return floor($mb/5) * 5;

    }

    public function MbUploadSpeed(){

        $mb = round($this->UploadSpeed / 1000);
        $output = floor($mb/5) * 5;
        if ($mb < 10){
            $output = floor($mb/1) * 1;
        }
        if ($mb < 1){
            $output = $this->UploadSpeed/1000;
        }
        return $output;

    }

    public function PriceEuro($price){

        $output = str_replace('.', ',', $price);

        return "€ ".$output;

    }

}