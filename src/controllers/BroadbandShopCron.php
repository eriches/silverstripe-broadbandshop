<?php

use SilverStripe\CMS\Model\SiteTree;
use SilverStripe\ORM\DB;
use SilverStripe\Control\Email\Email;
use Hestec\BroadbandShop\BroadbandShopSubscription;
use Hestec\BroadbandShop\BroadbandShopSupplier;
use SilverStripe\Core\Config\Config;

class BroadbandShopCron extends \SilverStripe\Control\Controller {

    private static $allowed_actions = array (
        'UpdateBroadbandShop',
        'test'
    );

    public function test(){

        if (in_array($_SERVER['REMOTE_ADDR'], Config::inst()->get(__CLASS__, 'allowed_ips'))) {

            $client = new \GuzzleHttp\Client();
            $response = $client->request('GET', 'http://www.breedbandwinkel.nl/xml/api/aanbieders.xml', [
                'curl' => [
                    CURLOPT_IPRESOLVE => CURL_IPRESOLVE_V4
                ]
            ]);
            $xml = simplexml_load_string($response->getBody());

            if ($xml->error){
                return $xml->error;
            }

            $suppliers = "<h1>This are the available suppliers:</h1>";
            foreach ($xml->aanbieder as $node) {

                $suppliers .= $node."<br>";

            }

            return $suppliers;

        }else{

            return "no valid ip, add the ip(s) in your mysite.yml, see the readme.";

        }

    }

    public function UpdateBroadbandShop() {

        if (in_array($_SERVER['REMOTE_ADDR'], Config::inst()->get(__CLASS__, 'allowed_ips'))) {


            $this->UpdateBroadbandShopSuppliers();
            $this->UpdateBroadbandShopSubscriptions();

            return "updated";

        }

        return "no valid ip, add the ip(s) in your mysite.yml, see the readme.";

    }

    public function UpdateBroadbandShopSubscriptions() {

        $client = new \GuzzleHttp\Client();
        $response = $client->request('GET', 'http://www.breedbandwinkel.nl/xml/api/abonnementen.xml', [
            'curl' => [
                CURLOPT_IPRESOLVE => CURL_IPRESOLVE_V4
            ]
        ]);

        $xml_main = simplexml_load_string($response->getBody());

        // set StillInApi to 0, when the subscription id is returned from the api it will be set to 1, so all the subscriptions which are not in api anymore can be filter out
        DB::query('UPDATE BroadbandShopSubscription SET StillInApi = 0');

        // all-in-one, internet + tv, internet + phone
        foreach ($xml_main->pakketten->abonnement as $node) {

            $this->getSubscriptionData($node);

        }

        // internet only
        foreach ($xml_main->internet->abonnement as $node) {

            $this->getSubscriptionData($node);

        }

    }

    public function UpdateBroadbandShopSuppliers() {

        $client = new \GuzzleHttp\Client();
        $response = $client->request('GET', 'http://www.breedbandwinkel.nl/xml/api/aanbieders.xml', [
            'curl' => [
                CURLOPT_IPRESOLVE => CURL_IPRESOLVE_V4
            ]
        ]);

        $xml = simplexml_load_string($response->getBody());

        // set StillInApi to 0, when the subscription id is returned from the api it will be set to 1, so all the subscriptions which are not in api anymore can be filter out
        DB::query('UPDATE BroadbandShopSupplier SET StillInApi = 0');

        foreach ($xml->aanbieder as $node) {

            $supplier_id = $node['id']->__toString();

            $ts = $node['aangepast'];
            $dt = new \DateTime("@$ts");

            $count = BroadbandShopSupplier::get()->filter('SupplierId', $supplier_id)->count();

            if ($count == 0){

                $sup = new BroadbandShopSupplier();

            }else{

                $sup = BroadbandShopSupplier::get()->filter('SupplierId', $supplier_id)->first();

            }

            $sup->SupplierId = $supplier_id;
            $sup->StillInApi = 1; // see the DB::query above
            $sup->Name = $node->__toString();
            $sup->LastUpdate = $dt->format('Y-m-d H:i');

            $client = new \GuzzleHttp\Client();
            $response = $client->request('GET', 'http://www.breedbandwinkel.nl/xml/api/aanbieder.xml?id='.$supplier_id, [
                'curl' => [
                    CURLOPT_IPRESOLVE => CURL_IPRESOLVE_V4
                ]
            ]);
            $xml = simplexml_load_string($response->getBody());

            $sup->Description = $xml->omschrijving->__toString();
            $sup->CustomerServiceAddress = $xml->klantenservice->adres->__toString();
            $sup->CustomerServicePhone = $xml->klantenservice->telefonisch->telefoonnummer ->__toString();
            $sup->CustomerServiceHours = $xml->klantenservice->telefonisch->openingstijden ->__toString();
            $sup->ServiceAdsl = $xml->diensten->adsl['beschikbaar']->__toString();
            $sup->ServiceVdsl = $xml->diensten->vdsl['beschikbaar']->__toString();
            $sup->ServiceCable = $xml->diensten->kabel['beschikbaar']->__toString();
            $sup->ServiceFiber = $xml->diensten->glasvezel['beschikbaar']->__toString();
            $sup->ServicePhone = $xml->diensten->telefonie['beschikbaar']->__toString();
            $sup->ServiceTv = $xml->diensten->digitaletelevisie['beschikbaar']->__toString();

            $sup->write();

        }

    }

    public function makeArray($input) {

        $output = array();
        foreach ($input as $node) {

            $output[] = $node->__toString();

        }

        return $output;

    }

    public function getSubscriptionData($node) {

        $subscription_id = $node['id']->__toString();

        $ts = $node['aangepast'];
        $dt = new \DateTime("@$ts");

        $count = BroadbandShopSubscription::get()->filter('SubscriptionId', $subscription_id)->count();

        if ($count == 0){

            $sub = new BroadbandShopSubscription();

        }else{

            $sub = BroadbandShopSubscription::get()->filter('SubscriptionId', $subscription_id)->first();

        }

        $sub->SubscriptionId = $subscription_id;
        $sub->StillInApi = 1; // see the DB::query above
        $sub->Title = $node->__toString();
        $sub->LastUpdate = $dt->format('Y-m-d H:i');
        //$sub->ComparisonAllInOnePageID = ComparisonAllInOnePage::get()->first()->ID;
        $sub->UrlId = SiteTree::create()->generateURLSegment($node."-".$subscription_id);

        $client = new \GuzzleHttp\Client();
        $response = $client->request('GET', 'http://www.breedbandwinkel.nl/xml/api/abonnement.xml?id='.$subscription_id, [
            'curl' => [
                CURLOPT_IPRESOLVE => CURL_IPRESOLVE_V4
            ]
        ]);
        $xml = simplexml_load_string($response->getBody());

        if ($xml->diensten->internet['beschikbaar'] == 1){

            $sub->OfferInternet = $this->makeArray($xml->diensten->internet->aanbiedingen->aanbieding);

            $sub->NetworkType = $xml->diensten->internet->netwerk['signaal']->__toString();
            $sub->SpeedUnit = $xml->diensten->internet->snelheid['eenheid']->__toString();
            $sub->DownloadSpeed = $xml->diensten->internet->snelheid->download->__toString();
            $sub->UploadSpeed = $xml->diensten->internet->snelheid->upload->__toString();

        }

        if ($xml->diensten->telefonie['beschikbaar'] == 1){

            $sub->OfferPhone = $this->makeArray($xml->diensten->telefonie->aanbiedingen->aanbieding);

            $sub->PhoneUnlimited = $xml->diensten->telefonie->onbeperkt->__toString();

        }

        if ($xml->diensten->digitaletelevisie['beschikbaar'] == 1){

            $sub->OfferTv = $this->makeArray($xml->diensten->digitaletelevisie->aanbiedingen->aanbieding);

            $sub->TvChannelsTotal = $xml->diensten->digitaletelevisie->zenders->tv->__toString();
            $sub->TvChannelsHd = $xml->diensten->digitaletelevisie->zenders->hd->__toString();
            $sub->TvHd = $xml->diensten->digitaletelevisie->hdtv->__toString();
            $sub->TvRecord = $xml->diensten->digitaletelevisie->opnemen->__toString();
            $sub->TvMultipleTvs = $xml->diensten->digitaletelevisie->meerderetvs->__toString();
            $sub->TvStartMissed = $xml->diensten->digitaletelevisie->begingemist->__toString();
            $sub->TvProgramMissed = $xml->diensten->digitaletelevisie->programmagemist->__toString();
            $sub->TvVod = $xml->diensten->digitaletelevisie->vod->__toString();

        }

        $sub->SupplierId = $xml->aanbieder['id']->__toString();
        $sub->Category = $xml->product['categorie']->__toString();
        $sub->ContractDuration = $xml->contract->contractduur->__toString()." ".$xml->contract->contractduur['eenheid']->__toString();
        $sub->OfferExclusive = $xml->aanbiedingen['exclusief']->__toString();
        $sub->Discount = $xml->tarieven->korting->__toString();

        $lineprice = 0;
        $offer_type = "intro";
        $plus_yearprice = 0;
        if ($xml->aansluitingen->kpn->tarieven->maandelijks->__toString() > 0){
            $lineprice = $xml->aansluitingen->kpn->tarieven->maandelijks->__toString();
            $offer_type = "lijnkosten";
            $plus_yearprice = 12 * $lineprice;
        }
        if ($xml->aansluitingen->kabel->tarieven->maandelijks->__toString() > 0){
            $lineprice = $xml->aansluitingen->kabel->tarieven->maandelijks->__toString();
            $offer_type = "lijnkosten";
            $plus_yearprice = 12 * $lineprice;
        }

        foreach ($xml->aanbiedingen->aanbieding as $node) {

            switch($node['versie']){
                case $offer_type:
                    $sub->OfferIntro = $node->__toString();
                    break;
                case "overig":
                    $sub->OfferExtra = $node->__toString();
                    break;
                case "kort":
                    $sub->OfferShort = $node->__toString();
                    break;
            }

        }

        $sub->BasePrice = $xml->tarieven->maandelijks->standaardtarief->__toString() + $lineprice;
        $sub->YearPrice = $xml->tarieven->totaal->__toString() + $plus_yearprice;
        ;
        $sub->Category = $xml->product['categorie']->__toString();

        $baseprice = $xml->tarieven->maandelijks->standaardtarief->__toString();
        $sub->DiscountPrice = 0; // set to null before update
        $sub->DiscountDuration = 0; // set to null before update
        foreach ($xml->tarieven->maandelijks->opbouw->tarief as $node) {

            if ($node->__toString() < $baseprice) {

                $sub->DiscountPrice = $node->__toString() + $lineprice;
                $sub->DiscountDuration = $node['termijn']->__toString();

            }

        }

        $supplier = BroadbandShopSupplier::get()->filter('SupplierId', $xml->aanbieder['id'])->first();
        $sub->BroadbandShopSupplierID = $supplier->ID;

        $sub->write();

    }

}
